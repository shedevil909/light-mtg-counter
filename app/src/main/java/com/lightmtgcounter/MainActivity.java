package com.lightmtgcounter;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String COUNTERS_STATES = "COUNTERS_STATES";
    private List<Counter> _counters = new ArrayList<>(2);
    private TextView _counterFirst;
    private TextView _counterSecond;
    private int _loseColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
            actionBar.hide();

        _counters.add(new Counter(20));
        _counters.add(new Counter(20));

        _counterFirst = (TextView) findViewById(R.id.counterFirst);
        _counterSecond = (TextView) findViewById(R.id.counterSecond);

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/RobotoLight.ttf");

        _counterFirst.setTypeface(custom_font);
        _counterSecond.setTypeface(custom_font);

        _loseColor = ContextCompat.getColor(getApplicationContext(), R.color.colorLose);

        _counterFirst.setText(String.valueOf(20));
        _counterSecond.setText(String.valueOf(20));

        if(savedInstanceState != null){
            int[] counters_states = savedInstanceState.getIntArray(COUNTERS_STATES);

            for(int i = 0; i < counters_states.length; i++){
                _counters.get(i).SetCount(counters_states[i]);
            }

            _counterFirst.setText(String.valueOf(Math.abs(_counters.get(0).GetCount())));
            _counterSecond.setText(String.valueOf(Math.abs(_counters.get(1).GetCount())));

            _counterFirst.setTextColor(_counters.get(0).GetCount() > 0 ? Color.WHITE : _loseColor);
            _counterSecond.setTextColor(_counters.get(1).GetCount() > 0 ? Color.WHITE : _loseColor);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        int[] counters_states = new int[_counters.size()];

        for(int i = 0; i < counters_states.length; i++){
            counters_states[i] = _counters.get(i).GetCount();
        }

        outState.putIntArray(COUNTERS_STATES, counters_states);
    }

    public void onClick(View view){

        switch (view.getId()){
            case R.id.decreaseFirst:
                _counterFirst.setText(String.valueOf(Math.abs(_counters.get(0).Decrease())));
                break;
            case R.id.increaseFirst:
                _counterFirst.setText(String.valueOf(Math.abs(_counters.get(0).Increase())));
                break;
            case R.id.decreaseSecond:
                _counterSecond.setText(String.valueOf(Math.abs(_counters.get(1).Decrease())));
                break;
            case R.id.increaseSecond:
                _counterSecond.setText(String.valueOf(Math.abs(_counters.get(1).Increase())));
                break;
            default:
                break;
        }

        _counterFirst.setTextColor(_counters.get(0).GetCount() > 0 ? Color.WHITE : _loseColor);
        _counterSecond.setTextColor(_counters.get(1).GetCount() > 0 ? Color.WHITE : _loseColor);
    }
}
