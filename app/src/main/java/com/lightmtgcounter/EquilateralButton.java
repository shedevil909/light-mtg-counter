package com.lightmtgcounter;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class EquilateralButton extends Button {
    public EquilateralButton(Context context) {
        super(context);
    }

    public EquilateralButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EquilateralButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (getBackground() != null)
        {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            int height = width * getBackground().getIntrinsicHeight() / getBackground().getIntrinsicWidth();
            setMeasuredDimension(width, height);
        }
    }
}
