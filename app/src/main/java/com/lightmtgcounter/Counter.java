package com.lightmtgcounter;

public class Counter {

    private int _count;

    public Counter(){
        _count = 0;
    }

    public Counter(int initialCount){
        _count = initialCount;
    }

    public int GetCount(){
        return _count;
    }

    public void SetCount(int value){ _count = value; }

    public int Increase(){
        _count += 1;
        return _count;
    }

    public int Decrease(){
        _count -= 1;

        return _count;
    }
}
